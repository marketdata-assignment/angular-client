export class MarketData {

  // brand of MarketData
  _brand: string;

  // Date of MarketData
  _date: Date;

  // Open price of MarketData
  _open: number;

  // High price of MarketData
  _high: number;

  // Low price of MarketData
  _low: number;

  // Close price of MarketData
  _close: number;

  // Constructor with params
  MarketData(brand: string, date: Date, open: number, high: number, low: number, close: number) {
    this._brand = brand;
    this._date = date;
    this._open = open;
    this._high = high;
    this._low = low;
    this._close = close;
  }
}
