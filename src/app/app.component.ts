import {Component, ElementRef} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MarketData} from './model/MarketData';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Assignment Market Data';
  filename = '';
  firstMarket: Array<MarketData> = [];
  secondMarket: Array<MarketData> = [];
  comparisonMarket: Array<MarketData> = [];

  constructor(private http: HttpClient, private elem: ElementRef) {
    this.getData();
  }

  getData(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Headers': 'Origin, Content-Type',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Origin': '*'
      })
    };
    const url = 'http://localhost:51276/api/marketdata';
    // marketdata load for APPL
    return this.http.get(url, httpOptions).subscribe(
      data => {
        for (const market in data[0]) {
          this.firstMarket.push(data[0][market]);

        }
        for (const market in data[1]) {
          this.secondMarket.push(data[1][market]);
        }
      },
      error => {
        console.log('error');
      }
    );
  }

  comparison(): any {
    for (let index = 0; index < this.firstMarket.length; index++) {
      const marketData = new MarketData();
      marketData._date = this.firstMarket[index]._date;
      marketData._open = this.firstMarket[index]._open - this.secondMarket[index]._open;
      marketData._high = this.firstMarket[index]._high - this.secondMarket[index]._high;
      marketData._low = this.firstMarket[index]._low - this.secondMarket[index]._low;
      marketData._close = this.firstMarket[index]._close - this.secondMarket[index]._close;
      this.comparisonMarket.push(marketData);
    }
  }

  loadData(jsonObject): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Headers': 'Origin, Content-Type',
        'Access-Control-Allow-Methods': 'POST',
        'Access-Control-Allow-Origin': '*'
      })
    };
    const url = 'http://localhost:51276/api/marketdata';
    // marketdata load for APPL
    this.http.post(url, jsonObject, httpOptions).subscribe(
      data => {
        location.reload();
      },
      error => {
        console.log('error');
      }
    );
  }

  importFile(): void {
    const files = this.elem.nativeElement.querySelector('#selectFile').files;
    const file = files[0];


    if (!file) {
      alert('You need to select a file to import !');
    } else {
      this.filename = file.name.substr(0, file.name.length - 4);
      if (this.filename === 'AAPL' || this.filename === 'S&P500') {
        const reader = new FileReader();

        reader.readAsText(file);

        reader.onload = this.loadHandler.bind(this);
        reader.onerror = this.errorHandler;
      } else {
        alert('Only AAPL.csv and S&P500.csv accepted !');
      }

    }
  }

  loadHandler(event): any {

    const csv = event.target.result;
    const jsonObject = [];

    const allRows = csv.split(/\r?\n|\r/);
    for (let singleRow = 1; singleRow < allRows.length - 1; singleRow++) {
      const rowCells = allRows[singleRow].split(',');
      const marketData = new MarketData();
      marketData._brand = this.filename;
      marketData._date = rowCells[0];
      marketData._open = rowCells[1];
      marketData._high = rowCells[2];
      marketData._low = rowCells[3];
      marketData._close = rowCells[4];
      jsonObject.push(marketData);
    }
    console.log(jsonObject);
    this.loadData(jsonObject);
  }

  errorHandler(evt) {
    if (evt.target.error.name === 'NotReadableError') {
      alert('Cannot read file !');
    }
  }

}
